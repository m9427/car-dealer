// const inventory = require('./car_inventory');
const problem4 = require('./problem4');


function problem5(inventory,year)
{
    // let carscount=0;
    const carsCheck={
        carsCount:0,
        carList: [],
    };
    let emptyArr=[];
    if(inventory===undefined || inventory.length===0 || year===undefined || year<0)
    {
        // console.log('Empty input');
        return emptyArr;
    }

    for(let i=0;i<problem4(inventory).length;i++)
    {
        if(problem4(inventory)[i]<year)
        {
            carsCheck.carsCount++;
            carsCheck.carList.push(inventory[i]);
        }
    }
    return carsCheck.carList;
}

//  console.log('Number of cars older that 2010 are: '+problem5(inventory,2010).carscount);

module.exports=problem5;