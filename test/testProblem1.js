const problem1 = require('../problem1');
const inventory=require('../car_inventory');

function testProblem1(inputInventory,inputId)
{
    const res= problem1(inputInventory,inputId);
    // if car searched for is not there.
    /*
    if(res===undefined)
    {
        console.log(res);
    }
    else
    */
    {
        console.log(`Car ${res.id} is a ${res.car_year} ${res.car_make} ${res.car_model}.`);
    }
}


testProblem1(inventory,2);
testProblem1([]);
testProblem1([],3);
testProblem1(inventory);
testProblem1(inventory,-1);
testProblem1();



// // readline is an interface to get user input from nodeJS console. importing readline module
// const readline = require('readline');

// // creating interface ques with input and output
// const ques = readline.createInterface({
//     input: process.stdin,
//     output: process.stdout,
// });

// // using question method of the interface readline to take input and return output to the console
// ques.question("Enter the id to be searched in the inventory ? id: ", function(ans){
//     console.log(problem1(inventory,parseInt(ans)));
//     // closing the interface
//     ques.close();
// }); 


//console.log(findById(1));