const inventory = require('../car_inventory');
const problem3 = require('../problem3');

// console.log('The car models arranged in the alphabetical order are: ');
function testProblem3(inputInventory)
{
    console.log(problem3(inputInventory));
}

testProblem3(inventory);
testProblem3([]);
testProblem3();