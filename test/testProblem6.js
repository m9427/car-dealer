const inventory = require('../car_inventory');
const problem6 = require('../problem6');

function testProblem6(inputInventory)
{
    console.log(problem6(inputInventory));
}

testProblem6(inventory);
testProblem6([]);
testProblem6();