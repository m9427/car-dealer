const inventory = require('../car_inventory');
const problem4 = require('../problem4');

function testProblem4(inputInventory)
{
    console.log(problem4(inputInventory));
}

testProblem4(inventory);
testProblem4([]);
testProblem4();