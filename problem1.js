// const inventory = require('./car_inventory');

// we can use array.find method but here we will try and get it via for loop.
// const res= inventory.find((x)=>x.id===4);

// added for-loop to search within an array and find the object via id, x here is the id to be searched
function problem1(inventory,inputId)
{
    let emptyArr=[];
    if(inventory===undefined || inventory.length===0 || inputId===undefined || inputId<0)
    {
        // console.log('Empty input');
        return emptyArr;
    }
   for(let index=0; index<inventory.length;index++)
   {
       if(inventory[index].id===inputId)
       {
           return inventory[index];
       }
   }
   return emptyArr;
} 
// we take the input x from user and pass on the findById function as output


// console.log (problem1(inventory,11));

module.exports = problem1;

